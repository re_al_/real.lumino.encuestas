﻿// // <copyright file="CsvWriter.cs" company="INTEGRATE - Soluciones Informaticas">
// // Copyright (c) 2016 Todos los derechos reservados
// // </copyright>
// // <author>re-al </author>
// // <date>2018-02-25 17:44</date>

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace ReAl.Lumino.Encuestas.Helpers
{
    public class CsvWriter
    {
        private const string Delimiter = "|";

        public string Write<T>(IList<T> list, bool includeHeader = true)
        {
            var sb = new StringBuilder();

            var type = typeof(T);

            var properties = type.GetProperties();

            if (includeHeader)
            {
                sb.AppendLine(CreateCsvHeaderLine(properties));
            }

            foreach (var item in list)
            {
                sb.AppendLine(CreateCsvLine(item, properties));
            }

            return sb.ToString();
        }

        public string Write<T>(IList<T> list, string fileName, bool includeHeader = true)
        {
            var csv = Write(list, includeHeader);

            WriteFile(fileName, csv);

            return csv;
        }

        private Boolean ExportProperty(PropertyInfo prop)
        {
            //return prop.GetType().IsPrimitive;
            return prop.PropertyType.IsValueType ||
                   prop.PropertyType.IsPrimitive ||
                   prop.PropertyType == typeof(String) ||
                   prop.PropertyType == typeof(Char) ||
                   prop.PropertyType == typeof(Guid) ||
                   
                   prop.PropertyType == typeof(Boolean) ||
                   prop.PropertyType == typeof(Byte) ||
                   prop.PropertyType == typeof(Int16) ||
                   prop.PropertyType == typeof(Int32) ||
                   prop.PropertyType == typeof(Int64) ||
                   prop.PropertyType == typeof(Single) ||
                   prop.PropertyType == typeof(Double) ||
                   prop.PropertyType == typeof(Decimal) ||
                   
                   prop.PropertyType == typeof(SByte) ||
                   prop.PropertyType == typeof(UInt16) ||
                   prop.PropertyType == typeof(UInt32) ||
                   prop.PropertyType == typeof(UInt64) ||
                   
                   prop.PropertyType == typeof(DateTime) ||
                   prop.PropertyType == typeof(DateTimeOffset) ||
                   prop.PropertyType == typeof(TimeSpan) 
                ;
        }
        
        private string CreateCsvHeaderLine(IEnumerable<PropertyInfo> properties)
        {
            var propertyValues = new List<string>();

            foreach (var prop in properties)
            {
                if (ExportProperty(prop))
                {
                    var value = prop.Name;

                    var attribute = prop.GetCustomAttribute(typeof(DisplayAttribute));
                    if (attribute != null)
                    {
                        value = (attribute as DisplayAttribute)?.Name;
                    }

                    CreateCsvStringItem(propertyValues, value);                    
                }                
            }

            return CreateCsvLine(propertyValues);
        }

        private string CreateCsvLine<T>(T item, IEnumerable<PropertyInfo> properties)
        {
            var propertyValues = new List<string>();

            foreach (var prop in properties)
            {
                if (ExportProperty(prop))
                {
                    var value = prop.GetValue(item, null);

                    if (prop.PropertyType == typeof(string))
                    {
                        CreateCsvStringItem(propertyValues, value);
                    }
                    else if (prop.PropertyType == typeof(string[]))
                    {
                        CreateCsvStringArrayItem(propertyValues, value);
                    }
                    else if (prop.PropertyType == typeof(List<string>))
                    {
                        CreateCsvStringListItem(propertyValues, value);
                    }
                    else
                    {
                        CreateCsvItem(propertyValues, value);
                    }
                }                
            }

            return CreateCsvLine(propertyValues);
        }

        private string CreateCsvLine(IEnumerable<string> list)
        {
            return string.Join(Delimiter, list);
        }

        private void CreateCsvItem(ICollection<string> propertyValues, object value)
        {
            if (value != null)
            {
                propertyValues.Add(value.ToString());
            }
            else
            {
                propertyValues.Add(string.Empty);
            }
        }

        private void CreateCsvStringListItem(List<string> propertyValues, object value)
        {
            //var formatString = "\"{0}\"";
            var formatString = "{0}";
            if (value != null)
            {
                value = CreateCsvLine((List<string>) value);
                propertyValues.Add(string.Format(formatString, ProcessStringEscapeSequence(value)));
            }
            else
            {
                propertyValues.Add(string.Empty);
            }
        }

        private void CreateCsvStringArrayItem(List<string> propertyValues, object value)
        {
            //var formatString = "\"{0}\"";
            var formatString = "{0}";
            if (value != null)
            {
                value = CreateCsvLine(((string[]) value).ToList());
                propertyValues.Add(string.Format(formatString, ProcessStringEscapeSequence(value)));
            }
            else
            {
                propertyValues.Add(string.Empty);
            }
        }

        private void CreateCsvStringItem(List<string> propertyValues, object value)
        {
            //var formatString = "\"{0}\"";
            var formatString = "{0}";
            if (value != null)
            {
                propertyValues.Add(string.Format(formatString, ProcessStringEscapeSequence(value)));
            }
            else
            {
                propertyValues.Add(string.Empty);
            }
        }

        private string ProcessStringEscapeSequence(object value)
        {
            return value.ToString().Replace("\"", "\"\"");
        }

        public bool WriteFile(string fileName, string csv)
        {
            var fileCreated = false;

            if (!string.IsNullOrWhiteSpace(fileName))
            {
                File.WriteAllText(fileName, csv);

                fileCreated = true;
            }

            return fileCreated;
        }
    }
}