﻿// // <copyright file="TableResultHelper.cs" company="INTEGRATE - Soluciones Informaticas">
// // Copyright (c) 2016 Todos los derechos reservados
// // </copyright>
// // <author>re-al </author>
// // <date>2018-03-30 7:49</date>

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;

namespace ReAl.Lumino.Encuestas.Helpers
{
    //KB: http://dotnetdevtricks.blogspot.com/2016/06/dynamic-columns-for-table-in-aspnet-mvc.html    
    public static class TableResultHelper
    {
        public static DataTable ToDataTable<T>(this IList<T> data)  
        {  
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));  
            DataTable table = new DataTable();  
            foreach (PropertyDescriptor prop in properties)  
            {  
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);  
            }  
            foreach (T item in data)  
            {  
                DataRow row = table.NewRow();  
                foreach (PropertyDescriptor prop in properties)  
                {  
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;  
                }  
                table.Rows.Add(row);  
            }  
            return table;  
        }
    }
}