﻿// // <copyright file="ZipController.cs" company="INTEGRATE - Soluciones Informaticas">
// // Copyright (c) 2016 Todos los derechos reservados
// // </copyright>
// // <author>re-al </author>
// // <date>2018-02-25 17:42</date>

using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Configuration;
using ReAl.Lumino.Encuestas.Helpers;
using ReAl.Lumino.Encuestas.Models;
using Serilog;

namespace ReAl.Lumino.Encuestas.Controllers
{
    public class ZipController : BaseController
    {
        public ZipController(db_encuestasContext context, IConfiguration configuration) : base(context, configuration)
        {
        }
        
        // GetZip/IMEI
        [HttpGet]
        [Route("Zip/GetZip/{id}")]
        public IActionResult GetZip(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            
            //Se busca el IMEI segun el ID
            var objUsuario = _context.SegUsuarios.SingleOrDefault(app => app.Tablet.Equals(id));
            if (objUsuario == null)
            {
                return BadRequest();
            }
            
            //Se busca el proyecto activo segun el Rol
            var objUsuarioRest = _context.SegUsuariosRestriccion
                .Where(sur1 => sur1.Idsus == objUsuario.Idsus)
                .SingleOrDefault(sur2 => sur2.Rolactivo == 1);
            if (objUsuarioRest == null)
            {
                return BadRequest();
            }
            
            Log.Information("Se está creando los archivos para " + id);
            //Creamos los CSVs
            var csvWriter = new CsvWriter();
            
            //SegUsuarios
            var lisSus = _context.SegUsuarios.Where(usu => usu.Login.Equals(objUsuario.Login)).ToList();                        
            csvWriter.Write(lisSus, "files/"+ SegUsuarios.StrNombreTabla.ToLower() +".csv");

            //SegUsuariosRestriccion
            var lisSur = _context.SegUsuariosRestriccion
                .Where(usu => usu.Idsus == objUsuario.Idsus)
                .Where(usu2 => usu2.Rolactivo == 1)
                .ToList();                        
            csvWriter.Write(lisSur, "files/"+ SegUsuariosRestriccion.StrNombreTabla.ToLower() +".csv");
            
            //SegRoles
            var lisSro = _context.SegRoles.ToList();                        
            csvWriter.Write(lisSro, "files/"+ SegRoles.StrNombreTabla.ToLower() +".csv");
            
            //OpeBrigadas
            var lisObr = _context.OpeBrigadas.Where(obr => obr.Idopy == objUsuarioRest.Idopy).ToList();                        
            csvWriter.Write(lisObr, "files/"+ OpeBrigadas.StrNombreTabla.ToLower() +".csv");
            
            //OpeMovimientos
            var lisOmv = _context.OpeMovimientos.Where(omv => omv.IdoupNavigation.Idopy == objUsuarioRest.Idopy).ToList();                        
            csvWriter.Write(lisOmv, "files/"+ OpeMovimientos.StrNombreTabla.ToLower() +".csv");
            
            //OpeProyectos
            var lisOpy = _context.OpeProyectos.Where(opy => opy.Idopy == objUsuarioRest.Idopy).ToList();
            csvWriter.Write(lisOpy, "files/"+ OpeProyectos.StrNombreTabla.ToLower() +".csv");
            
            //OpeProyectos
            var lisOtb = _context.OpeTiposBoleta.Where(opy => opy.Idopy == objUsuarioRest.Idopy).ToList();
            csvWriter.Write(lisOtb, "files/"+ OpeTiposBoleta.StrNombreTabla.ToLower() +".csv");
            
            //OpeUpms
            var lisOup = _context.OpeUpms.Where(oup => oup.Idopy == objUsuarioRest.Idopy).ToList();
            csvWriter.Write(lisOup, "files/"+ OpeUpms.StrNombreTabla.ToLower() +".csv");
            
            //CatDepartamento
            var lisCde = _context.CatDepartamentos.ToList();
            csvWriter.Write(lisCde, "files/"+ CatDepartamentos.StrNombreTabla.ToLower() +".csv");
            
            //CatTiposPregunta
            var lisCtp = _context.CatTiposPregunta.ToList();
            csvWriter.Write(lisCtp, "files/"+ CatTiposPregunta.StrNombreTabla.ToLower() +".csv");
            
            //CatNiveles
            var lisCnv = _context.CatNiveles.ToList();
            csvWriter.Write(lisCnv, "files/"+ CatNiveles.StrNombreTabla.ToLower() +".csv");
            
            //EncPreguntas
            var lisEpr = _context.EncPreguntas.Where(epr => epr.Idopy == objUsuarioRest.Idopy).ToList();
            csvWriter.Write(lisEpr, "files/"+ EncPreguntas.StrNombreTabla.ToLower() +".csv");
            
            //EncRespuestas
            var lisEre = _context.EncRespuestas.Where(ere => ere.IdeprNavigation.Idopy == objUsuarioRest.Idopy).ToList();
            csvWriter.Write(lisEre, "files/"+ EncRespuestas.StrNombreTabla.ToLower() +".csv");
            
            //EncSecciones
            var lisEse = _context.EncSecciones.Where(ese => ese.Idopy == objUsuarioRest.Idopy).ToList();
            csvWriter.Write(lisEse, "files/"+ EncSecciones.StrNombreTabla.ToLower() +".csv");
            
            //EncFlujos
            var lisEfl = _context.EncFlujos.Where(efl => efl.Idopy == objUsuarioRest.Idopy).ToList();
            csvWriter.Write(lisEfl, "files/"+ EncFlujos.StrNombreTabla.ToLower() +".csv");
            
            byte[] zipped;
            using (var ms = new MemoryStream())
            {
                using (var archive = new ZipArchive(ms, ZipArchiveMode.Create, true))
                {
                    //Iteramos los archivos dentro del directorio files
                    foreach (string file in Directory.EnumerateFiles(
                        System.IO.Directory.GetCurrentDirectory() + "/files", 
                        "*" , 
                        SearchOption.AllDirectories) 
                    )
                    {
                        Log.Information("-- archivo " + Path.GetFileName(file) + " dentro del ZIP");
                        var fileBytes = System.IO.File.ReadAllBytes(file);
                        var zipArchiveEntry = archive.CreateEntry(Path.GetFileName(file), CompressionLevel.Fastest);
                        using (var zipStream = zipArchiveEntry.Open()) 
                            zipStream.Write(fileBytes, 0, fileBytes.Length);    
                    }
                }                
                zipped = ms.ToArray();               
            }

            var result = new FileContentResult(zipped, "application/zip")
            {
                FileDownloadName = id + ".zip"
            };
            return result; 
        }
    }
}