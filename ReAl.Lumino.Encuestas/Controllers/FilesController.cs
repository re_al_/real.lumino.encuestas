﻿// // <copyright file="FilesController.cs" company="INTEGRATE - Soluciones Informaticas">
// // Copyright (c) 2016 Todos los derechos reservados
// // </copyright>
// // <author>re-al </author>
// // <date>2018-03-21 21:43</date>

using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ReAl.Lumino.Encuestas.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [Consumes("application/json","application/json-patch+json", "multipart/form-data")]
    public class FilesController : Controller
    {
        // POST api/values
        [HttpPost]
        public async Task<IActionResult> PostProfilePicture(IFormFile file)
        {
            var stream = file.OpenReadStream();
            var name = file.FileName;

            if (file.Length > 0)
            {
                using (var newStream = new FileStream("uploads/" + name, FileMode.Create))
                {
                    await file.CopyToAsync(newStream);
                }
            }
            return Ok();
        }
    }
}