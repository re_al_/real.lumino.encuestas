﻿// // <copyright file="ReportesController.cs" company="INTEGRATE - Soluciones Informaticas">
// // Copyright (c) 2016 Todos los derechos reservados
// // </copyright>
// // <author>re-al </author>
// // <date>2017-12-29 9:24</date>

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using Npgsql;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using ReAl.Lumino.Encuestas.Dal;
using ReAl.Lumino.Encuestas.Helpers;
using ReAl.Lumino.Encuestas.Models;
using SpssLib.DataReader;
using SpssLib.SpssDataset;

namespace ReAl.Lumino.Encuestas.Controllers
{
    public class ReportesController : BaseController
    {
        public ReportesController(db_encuestasContext context, IConfiguration configuration, IOptions<ConnectionStringsSettings> connstring) : base(context, configuration, connstring)
        {
        }
        
        // GET
        public IActionResult Index()
        {
            var lstReportes = _context.OpeReportes
                .Where(ore => ore.Idopy == GetProyectoId())
                .ToList();
            ViewData["Idore"] = lstReportes;
            return View();
        }

        // GET
        public PartialViewResult GetReport(string id = "")  
        {  
            if (id == "")
                return PartialView("_IndexPartial");
            
            var rn = new RnVista(_connectionStringsSettings.Value);
            var arrColWhere = new ArrayList {OpeProyectos.Fields.Idopy.ToString()};
            var arrValWhere = new ArrayList {this.GetProyectoId()};
            var dtReporte = rn.ObtenerDatos(id, arrColWhere, arrValWhere);
            
            ViewBag.dtReporte = dtReporte;
            return PartialView("_IndexPartial", dtReporte);  
        }
        
        /// <summary>
        /// An in-memory report
        /// </summary>
        [HttpGet]
        [Route("Reportes/DownloadSpss/{id}")]
        public IActionResult DownloadSpss(string id = "")
        {
            if (id == "")
                return BadRequest();
            
            var rn = new RnVista(_connectionStringsSettings.Value);
            var arrColWhere = new ArrayList {OpeProyectos.Fields.Idopy.ToString()};
            var arrValWhere = new ArrayList {this.GetProyectoId()};
            var dtReporte = rn.ObtenerDatos(id, arrColWhere, arrValWhere);
            
            // Create Variable list
            var variables = new List<Variable>();

            foreach (DataColumn column in dtReporte.Columns)
            {
                var miVar = new Variable();
                miVar.Label = column.Caption;
                miVar.Name = column.ColumnName;
                miVar.PrintFormat = new OutputFormat(FormatType.A, 10);
                miVar.WriteFormat = new OutputFormat(FormatType.A, 512);
                miVar.Type = DataType.Text;
                miVar.Width = 15;
                miVar.TextWidth = 512;
                miVar.MissingValueType = MissingValueType.NoMissingValues;
                
                variables.Add(miVar);
            }            
            // Default options
            var options = new SpssOptions();
            
            byte[] reportBytes;
            //using (FileStream fileStream = new FileStream("data.sav", FileMode.Create, FileAccess.Write))
            using (var ms = new MemoryStream())
            {
                using (var writer = new SpssWriter(ms, variables, options))
                {
                    // Create and write records
                    foreach (DataRow row in dtReporte.Rows)
                    {
                        var newRecord = writer.CreateRecord();
                        var i = 0;
                        foreach (DataColumn column in dtReporte.Columns)
                        {
                            newRecord[i] = row[column.ColumnName].ToString().Trim();
                            i++;
                        }
                        writer.WriteRecord(newRecord);
                    }
                    writer.EndFile();
                }
                reportBytes = ms.ToArray();
            }
            
            return File(reportBytes, "application/x-spss", "data.sav");
        }
        
        /// <summary>
        /// An in-memory report
        /// </summary>
        [HttpGet]
        [Route("Reportes/Download/{id}")]
        public IActionResult Download(string id = "")
        {
            if (id == "")
                return BadRequest();
            
            byte[] reportBytes;
            var rn = new RnVista(_connectionStringsSettings.Value);
            var arrColWhere = new ArrayList {OpeProyectos.Fields.Idopy.ToString()};
            var arrValWhere = new ArrayList {this.GetProyectoId()};
            var dtReporte = rn.ObtenerDatos(id, arrColWhere, arrValWhere);

            foreach (DataColumn column in dtReporte.Columns)
            {
                column.ColumnName = column.ColumnName.ToPascalCase();
            }
            

            using (var package = new ExcelPackage())
            {
                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Reporte");
                //Titulos
                ws.Cells["A1"].Value = "Reporte: " + id;
                ws.Cells["A2"].Value = "Fecha: " + DateTime.Now;
                ws.Cells["A3"].Value = "Elaborado por " + this.GetUser().Nombres + " " + this.GetUser().Apellidos;
                ws.Cells[1, 1, 3, 1].Style.Font.Bold = true;
                
                //Reporte
                ws.Cells["A6"].LoadFromDataTable(dtReporte, true);                
                ws.Cells[6, 1, dtReporte.Rows.Count, dtReporte.Columns.Count].AutoFitColumns();
                
                var tbl = ws.Tables.Add(new ExcelAddressBase(fromRow: 6, fromCol: 1, toRow: dtReporte.Rows.Count, toColumn: dtReporte.Columns.Count), "Data");
                tbl.ShowHeader = true;                
                
                reportBytes = package.GetAsByteArray();                 
            }

            return File(reportBytes, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "report.xlsx");
        }       
    }
}