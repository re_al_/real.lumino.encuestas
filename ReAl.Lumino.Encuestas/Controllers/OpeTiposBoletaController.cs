using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Npgsql;
using ReAl.Lumino.Encuestas.Models;

namespace ReAl.Lumino.Encuestas.Controllers
{
    [Authorize]
    public class OpeTiposBoletaController : BaseController
    {
		public OpeTiposBoletaController(db_encuestasContext context, IConfiguration configuration) : base(context, configuration)
        {
        }

        // GET: OpeTiposBoleta
        public async Task<IActionResult> Index()
        {
            var db_encuestasContext = _context.OpeTiposBoleta.Where(proy => proy.Idopy == GetProyectoId())
                .Include(o => o.IdeprNavigation).Include(o => o.IdopyNavigation);
            return View(await db_encuestasContext.ToListAsync());
        }

        // GET: OpeTiposBoleta/Details/5
        public async Task<IActionResult> Details(long? id)
        {
			
            if (id == null)
            {
                return NotFound();
            }

            var opeTiposBoleta = await _context.OpeTiposBoleta
                .Include(o => o.IdeprNavigation)
                .Include(o => o.IdopyNavigation)
                .SingleOrDefaultAsync(m => m.Idotb == id);
            if (opeTiposBoleta == null)
            {
                return NotFound();
            }

            return View(opeTiposBoleta);
        }

        // GET: OpeTiposBoleta/Create
        public IActionResult Create()
        {			
            ViewData["Idepr"] = new SelectList(_context.EncPreguntas.Where(epr => epr.Idopy == GetProyectoId()),
                EncPreguntas.Fields.Idepr.ToString(), 
                EncPreguntas.Fields.Codigo.ToString());
            ViewData["Idopy"] = new SelectList(_context.OpeProyectos.Where(proy => proy.Idopy == GetProyectoId()), 
                OpeProyectos.Fields.Idopy.ToString(), 
                OpeProyectos.Fields.Nombre.ToString());
                return View();
        }

        // POST: OpeTiposBoleta/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Idotb,Idopy,Idepr,TipoBoleta,Descripcion,Apiestado,Apitransaccion,Usucre,Feccre,Usumod,Fecmod")] OpeTiposBoleta opeTiposBoleta)
        {
			if (ModelState.IsValid)
            {
				try
				{
                    opeTiposBoleta.Usucre = this.GetLogin();
					_context.Add(opeTiposBoleta);
					await _context.SaveChangesAsync();
                	return RedirectToAction(nameof(Index));
				}
				catch (Exception exp)
                {
                    if (exp.InnerException is NpgsqlException)
                        ViewBag.ErrorDb = exp.InnerException.Message;                        
                    else
                        ModelState.AddModelError("", exp.Message);
                    ViewData["Idepr"] = 
                        new SelectList(_context.EncPreguntas.Where(epr => epr.Idopy == GetProyectoId()),
                        EncPreguntas.Fields.Idepr.ToString(), 
                        EncPreguntas.Fields.Codigo.ToString());
                    ViewData["Idopy"] = 
                        new SelectList(_context.OpeProyectos.Where(proy => proy.Idopy == GetProyectoId()), 
                        OpeProyectos.Fields.Idopy.ToString(), 
                        OpeProyectos.Fields.Nombre.ToString());
                    return View();
                }  
            }
            ViewData["Idepr"] = new SelectList(_context.EncPreguntas.Where(epr => epr.Idopy == GetProyectoId()),
                EncPreguntas.Fields.Idepr.ToString(), 
                EncPreguntas.Fields.Codigo.ToString(), 
                opeTiposBoleta.Idepr);
            ViewData["Idopy"] = new SelectList(_context.OpeProyectos.Where(proy => proy.Idopy == GetProyectoId()), 
                OpeProyectos.Fields.Idopy.ToString(), 
                OpeProyectos.Fields.Nombre.ToString(), 
                opeTiposBoleta.Idopy);
            return View(opeTiposBoleta);
        }

        // GET: OpeTiposBoleta/Edit/5
        public async Task<IActionResult> Edit(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opeTiposBoleta = await _context.OpeTiposBoleta.SingleOrDefaultAsync(m => m.Idotb == id);
            if (opeTiposBoleta == null)
            {
                return NotFound();
            }
            ViewData["Idepr"] = new SelectList(_context.EncPreguntas.Where(epr => epr.Idopy == GetProyectoId()), 
                EncPreguntas.Fields.Idepr.ToString(), 
                EncPreguntas.Fields.Codigo.ToString(), 
                opeTiposBoleta.Idepr);
            ViewData["Idopy"] = new SelectList(_context.OpeProyectos.Where(proy => proy.Idopy == GetProyectoId()),
                OpeProyectos.Fields.Idopy.ToString(), 
                OpeProyectos.Fields.Nombre.ToString(), 
                opeTiposBoleta.Idopy);
            return View(opeTiposBoleta);
        }

        // POST: OpeTiposBoleta/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(long id, [Bind("Idotb,Idopy,Idepr,TipoBoleta,Descripcion,Apiestado,Apitransaccion,Usucre,Feccre,Usumod,Fecmod")] OpeTiposBoleta opeTiposBoleta)
        {
            if (id != opeTiposBoleta.Idotb)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    opeTiposBoleta.Usumod = this.GetLogin();
                    opeTiposBoleta.Apitransaccion = "MODIFICAR";
                    _context.Update(opeTiposBoleta);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!OpeTiposBoletaExists(opeTiposBoleta.Idotb))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
				catch (Exception exp)
                {
                    if (exp.InnerException is NpgsqlException)
                        ViewBag.ErrorDb = exp.InnerException.Message;                        
                    else
                        ModelState.AddModelError("", exp.Message);
                    ViewData["Idepr"] = 
                        new SelectList(_context.EncPreguntas.Where(epr => epr.Idopy == GetProyectoId()),
                        EncPreguntas.Fields.Idepr.ToString(), 
                        EncPreguntas.Fields.Codigo.ToString());
                    ViewData["Idopy"] = 
                        new SelectList(_context.OpeProyectos.Where(proy => proy.Idopy == GetProyectoId()),
                        OpeProyectos.Fields.Idopy.ToString(), 
                        OpeProyectos.Fields.Nombre.ToString());
                    return View(opeTiposBoleta);
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["Idepr"] = new SelectList(_context.EncPreguntas.Where(epr => epr.Idopy == GetProyectoId()),
                EncPreguntas.Fields.Idepr.ToString(), 
                EncPreguntas.Fields.Codigo.ToString(), 
                opeTiposBoleta.Idepr);
            ViewData["Idopy"] = new SelectList(_context.OpeProyectos.Where(proy => proy.Idopy == GetProyectoId()),
                OpeProyectos.Fields.Idopy.ToString(), 
                OpeProyectos.Fields.Nombre.ToString(), 
                opeTiposBoleta.Idopy);
            return View(opeTiposBoleta);
        }

        // GET: OpeTiposBoleta/Delete/5
        public async Task<IActionResult> Delete(long? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var opeTiposBoleta = await _context.OpeTiposBoleta
                .Include(o => o.IdeprNavigation)
                .Include(o => o.IdopyNavigation)
                .SingleOrDefaultAsync(m => m.Idotb == id);
            if (opeTiposBoleta == null)
            {
                return NotFound();
            }

            return View(opeTiposBoleta);
        }

        // POST: OpeTiposBoleta/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(long id)
        {
			try
			{
				var opeTiposBoleta = await _context.OpeTiposBoleta.SingleOrDefaultAsync(m => m.Idotb == id);
				_context.OpeTiposBoleta.Remove(opeTiposBoleta);
				await _context.SaveChangesAsync();
				return RedirectToAction(nameof(Index));
			}
			catch (Exception exp)
            {
                if (exp.InnerException is NpgsqlException)
                    ViewBag.ErrorDb = exp.InnerException.Message;                        
                else
                    ModelState.AddModelError("", exp.Message);
                ViewData["Idepr"] = new SelectList(_context.EncPreguntas.Where(epr => epr.Idopy == GetProyectoId()),
                    EncPreguntas.Fields.Idepr.ToString(), 
                    EncPreguntas.Fields.Codigo.ToString());
                ViewData["Idopy"] = new SelectList(_context.OpeProyectos.Where(proy => proy.Idopy == GetProyectoId()),
                    OpeProyectos.Fields.Idopy.ToString(), 
                    OpeProyectos.Fields.Nombre.ToString());
                return View();
            }     
        }

        private bool OpeTiposBoletaExists(long id)
        {
            return _context.OpeTiposBoleta.Any(e => e.Idotb == id);
        }
    }
}
