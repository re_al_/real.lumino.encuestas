﻿using System;
using System.Collections.Generic;

namespace ReAl.Lumino.Encuestas.Models
{
    public partial class OpeReportes
    {
        public long Idore { get; set; }
        public long Idopy { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public string Vista { get; set; }
        public string ColsOcultas { get; set; }
        public string Apiestado { get; set; }
        public string Apitransaccion { get; set; }
        public string Usucre { get; set; }
        public DateTime Feccre { get; set; }
        public string Usumod { get; set; }
        public DateTime? Fecmod { get; set; }

        public OpeProyectos IdopyNavigation { get; set; }
    }
}
